base   gitlabissues
author Joerg Hampel
email  joerg@hampel.at
date   2015-01-11
name   Gitlab API for Issues
desc   Shows issues from gitlab.com for a given project and user API token
url    https://gitlab.com/joerg.hampel/dokuwiki-gitlabapi/
