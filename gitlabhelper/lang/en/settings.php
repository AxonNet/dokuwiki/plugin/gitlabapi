<?php
/**
 * English language file
 *
 */

$lang['gitlab_url'] = 'URL of the Gitlab server API (default: https://gitlab.com/api/v3)';
$lang['api_token'] = 'Private token of the user that should access gitlab application resources';
$lang['release_url'] = 'URL to the directory containing released files<small><p>This is the base URL for serving files. An example download link could look like this:<br/><code>release_url/namespace/project/tag/file.zip</code></small>';
$lang['release_remote'] = 'Check for released files on a remote server?<small><p>gitlabhelper can either browse a directory on the same server that dokuwiki is installed on, or it can communicate with another server hosting the release files.</p><p>If you check this option, gitlabhelper will try to read files from the <code>release_url</code> defined above. There has to be a script at this location handling two requests:</p><ul><li><code>release_url/index.php?l=...</code> which returns a list of files for a given directory</li><li><code>release_url/index.php?d=...</code> which returns the file itself for download.</li></ul><p>A sample <code>index.php.sample</code> can be found in the gitlabhelper plugin directory.</p></small>';
$lang['remote_pubkey'] = 'If checking remotely: Public key for secure communication<small><p>This parameter is optional. If omitted, the URLs are base64 encoded</p></small>';
$lang['release_path'] = 'If checking locally: Server path to the directory containing released files<small><p>This is the directory that resembles the same location as the <code>release_url</code> above. An example file could reside at:<br/><code>release_path/namespace/project/tag/file.zip</code></p></small>';
$lang['use_namespace'] = 'If checking locally: Use namespace in path and url?<small><p>I.e. put it between the release_path / release_url and the project name?</p></small>';
